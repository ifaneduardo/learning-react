#Welcome to React

React is a Javascript library developed by Facebook
and based on components reusables for users interfaces.
It serves for creating web app and facilitate programming in JS.
A very important term in React is JSX, but... what is that?


#What is JSX?

JSX is a syntax of javascript for React similar to an HTML tag, but 
more powerful and complicated.
JSX is not a valid Javascript, so it has to be compiled before.

###Example of a element JSX:
const element = <h1> Hi! I am a Title:) </h1>;

A JSX element is like a JavaScript expression, and it can be saved in 
a variable, as a function, as an object or array, etc...

###Example of a element JSX on a object:
const kondoSoft = {
  ceo: <li>Cyberpolin</li>,
  developer: <li>Luis Suarez</li>,
  trainee: <li>Roberto Fuccu</li>,
  trainee2: <li>Ivan Gutierrez</li>,
};

JSX elements can have attributes like an HTML tag
Example:
const title = <h1 id='title'>example of attributes</h1>; 


###there is a golden rule
_JSX should have only one outermost element_
Example:
const paragraphs = (
  <div id="i-am-the-outermost-element">
    <p>I am a paragraph.</p>
    <p>I, too, am a paragraph.</p>
  </div>
);