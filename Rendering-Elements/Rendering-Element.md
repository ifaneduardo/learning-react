#Rendering Element

ReactDOM: is the name of a library of JS 
containing the methods that need React.

To render a JSX element, the syntax is the following:

ReactDOM.render(<h1>Hello world</h1>, 
document.getElementById('app'));

ReactDOM.render(): is the most common way to render.
<h1>Hello world</h1>: is the first parameter that is 
passed and is the one that appears on the screen.

document.getElementById('app'): is the second parameter, 
this adds the first parameter to the selected element.

You can also use a variable as a first parameter if it 
evaluates as a JSX expression.

Example:
const ListaDeberes = (
  <ol>
    <li>actualizar repo</li>
    <li>Junta en zoom</li>
  </ol>
);

ReactDOM.render(
  ListaDeberes, 
  document.getElementById('app')
